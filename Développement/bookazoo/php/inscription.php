<?php
	if(!empty($_POST['email']) && !empty($_POST['pseudo']) && !empty($_POST['mdp']) && !empty($_POST['confirmmdp']))
	{
		$email=$_POST['email'];
		$pseudo = $_POST['pseudo'];
		$mdp = $_POST['mdp'];
		$confirmmdp = $_POST['confirmmdp'];
		
		// Test de la présence de l'email dans la base de données
		include '../php/database.php';
		global $db;
		$query=$db->prepare('SELECT IDUSER, MAILUSER, PSEUDOUSER, MDPUSER FROM utilisateur WHERE MAILUSER = :email');
		$query->bindValue(':email',$email, PDO::PARAM_STR);
		$query->execute();
		$data=$query->fetch();
		
		if($data[0]=="")
		{
			$query=$db->prepare('INSERT INTO utilisateur(MAILUSER,PSEUDOUSER, MDPUSER) VALUES(:email, :pseudo, :mdp)');
			$query->bindValue(':email', $email, PDO::PARAM_STR);
			$query->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
			$query->bindValue(':mdp', $mdp, PDO::PARAM_STR);
			$query->execute();
			
			
			header('Location: ../html/connexion.html');
		}
		else
		{	
			echo 'Cet email est déjà utilisé';
			header('Location: ../html/inscription.html');
		}
		
	}
	else
	{
		// dans le cas où l'utilisateur retire les required des champs en allant dans l'inspecteur
		echo 'Veuillez remplir tous les champs';
		header('Location: ../html/inscription.html');
	}
?>