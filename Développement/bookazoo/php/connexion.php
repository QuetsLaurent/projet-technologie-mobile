<?php
        //On vérifie les champs si l'utilisateur a modifier le required
		 if (empty($_POST['email']) || empty($_POST['mdp']) ) //Oubli d'un champ
		{
			echo "Veuillez remplir tous les champs";
		}
		else //On vérifie le mot de passe
		{
			include '../php/database.php';
			global $db;
			$query=$db->prepare('SELECT IDUSER, MAILUSER, PSEUDOUSER, MDPUSER FROM utilisateur WHERE MAILUSER = :email');
			$query->bindValue(':email',$_POST['email'], PDO::PARAM_STR);
			$query->execute();
			$data=$query->fetch();

			if($data['MDPUSER'] == $_POST['mdp']) // Acces OK
			{
				session_start();
				$_SESSION['pseudo'] = $data['PSEUDOUSER'];
				$_SESSION['email'] = $data['MAILUSER'];
				$_SESSION['id'] = $data['IDUSER'];

				setcookie("pseudoCookie", $_SESSION['pseudo'],time()+365*24*60*60, "/" ,"localhost");
				setcookie("emailCookie", $_SESSION['email'],time()+365*24*60*60, "/", "localhost");
				setcookie("idCookie", $_SESSION['id'],time()+365*24*60*60, "/", "localhost");

				header('Location: ../html/connexion.html');
				header('Location: ../html/index.html');
			}
			else // Acces pas OK
			{
				// Partie admin
				if(strcmp($_POST['email'], "admin@hotmail.be")== 0 && strcmp($_POST['mdp'], "admin")== 0)
				{
					$_SESSION['email'] = $_POST['email'];
					setCookie("emailCookie", $_SESSION['email'],time()+365*24*60*60, "/", "localhost");
					header('Location: ../html/indexadmin.html');
				}
				else
				{
					header('Location: ../html/index.html');
					echo "L'email ou le mot de passe est/sont incorrect(s)";
				}
			}
		}



?>