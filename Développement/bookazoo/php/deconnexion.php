<?php
session_start();
session_destroy();

setcookie("pseudoCookie", '', time() - 3600, '/');
setcookie("emailCookie", '', time() - 3600, '/');
setcookie("idCookie", '', time() - 3600, '/');
setcookie("nbPointsCookie", '', time() - 3600, '/');
setcookie("tourCookie", '', time() - 3600, '/');
setcookie("idParcCookie", '', time() - 3600, '/');
setcookie("nbCommandes", '', time() - 3600, '/');
setcookie("nomParcCookie", '', time() - 3600, '/');
setcookie("prixAdulteCookie", '', time() - 3600, '/');
setcookie("prixEnfantCookie", '', time() - 3600, '/');
setcookie("totalCommande", '', time() - 3600, '/');
header('Location: ../html/index.html');
?>
