<?php header('Content-Type: text/html; charset=utf-8');

// Connexion à la bd
	$servername= "localhost";
	$username = "root";
	$password = "";
	$dbname = "bookazoo_projet";
try
{
	$db = new PDO("mysql:host=".$servername.";dbname=".$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
	die($e->getMessage());
}
// Préparation de la requête
$requete=$db->prepare("select * from zoo");

$requete->execute();

$result = $requete->fetchAll(PDO::FETCH_ASSOC);

$result = utf8_encode(json_encode($result));

// Affichage sur la page.php
echo $result;

?>