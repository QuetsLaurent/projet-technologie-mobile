<?php
	if(!empty($_POST['idzoo']))
	{
		$idzoo=$_POST['idzoo'];
		include 'database.php';
		global $db;

		$query=$db->prepare('DELETE FROM ZOO WHERE IDZOO = :idzoo');
		$query->bindValue(':idzoo', $idzoo, PDO::PARAM_INT);
		$query->execute();

		header('Location: ../html/supprimerZoo.html');
	}
	else
	{
		// si on retire avec l'inspecteur
		header('Location: ../html/supprimerZoo.html');
	}	
?>