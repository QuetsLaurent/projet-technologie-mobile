<?php
if(!empty($_COOKIE['pseudoCookie']))
{
	if(!empty($_POST['nbEnfant']) || !empty($_POST['nbAdulte']))
	{
		$idClient = $_COOKIE["idCookie"];
		$pseudoClient = $_COOKIE["pseudoCookie"];
		$idZoo = $_COOKIE["idZooCookie"];
		$prixEnfant = $_COOKIE['prixEnfantCookie'];
		$prixAdulte = $_COOKIE['prixAdulteCookie'];
		$nbEnfant = $_POST['nbEnfant'];
		$nbAdulte = $_POST['nbAdulte'];

		include '../php/database.php';
		global $db;
		$query=$db->prepare('INSERT INTO commande(IDZOO,IDUSER,PRIXTICKETENFANT,NOMBRETICKETENFANT,PRIXTICKETADULTE,NOMBRETICKETADULTE) VALUES(:idzoo, :iduser, :prixticketenfant, :nombreticketenfant, :prixticketadulte, :nombreticketadulte)');
        $query->bindValue(':idzoo', $idZoo, PDO::PARAM_INT);
        $query->bindValue(':iduser', $idClient, PDO::PARAM_INT);
        $query->bindValue(':prixticketenfant', $prixEnfant, PDO::PARAM_INT);
        $query->bindValue(':nombreticketenfant', $nbEnfant, PDO::PARAM_INT);
        $query->bindValue(':prixticketadulte', $prixAdulte, PDO::PARAM_INT);
        $query->bindValue(':nombreticketadulte', $nbAdulte, PDO::PARAM_INT);
        $query->execute();

		header('Location: ../html/panier.html');
	}
	else
	{
		header('Location: ../html/index.html');
	}
}
else
{
	header('Location: ../html/index.html');
}
?>