<?php
// Connexion à la bd
	$servername= "localhost";
	$username = "root";
	$password = "";
	$dbname = "bookazoo_projet";
try
{
	$db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
	
}
// Préparation de la requête
$requete=$db->prepare("select * from commande");

$requete->execute();

$result = $requete->fetchAll(PDO::FETCH_ASSOC);

$result = utf8_encode(json_encode($result));

// Affichage sur la page.php
echo $result;

?>