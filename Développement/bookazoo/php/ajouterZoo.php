<?php
	if(!empty($_POST['nomzoo']) && !empty($_POST['prixadultezoo']) && !empty($_POST['prixenfantzoo']) && !empty($_POST['payszoo']) && !empty($_POST['villezoo']) && !empty($_POST['urlzoo']) && !empty($_POST['imagezoo']) && !empty($_POST['cartezoo']))
	{
		$nomzoo=$_POST['nomzoo'];
		$descriptionzoo=$_POST['descriptionzoo'];
		$prixadultezoo = $_POST['prixadultezoo'];
		$prixenfantzoo = $_POST['prixenfantzoo'];
		$payszoo = $_POST['payszoo'];
		$villezoo = $_POST['villezoo'];
		$urlzoo = $_POST['urlzoo'];
		$imagezoo = $_POST['imagezoo'];
		$cartezoo = $_POST['cartezoo'];


		include '../php/database.php';
		global $db;

		$query=$db->prepare('INSERT INTO zoo(NOMZOO,DESCRIPTIONZOO,PRIXADULTEZOO,PRIXENFANTZOO,PAYSZOO,VILLEZOO,URLZOO,IMAGEZOO,CARTEZOO) VALUES(:nomzoo, :descriptionzoo, :prixadultezoo, :prixenfantzoo, :payszoo, :villezoo, :urlzoo, :imagezoo, :cartezoo)');
		$query->bindValue(':nomzoo', $nomzoo, PDO::PARAM_STR);
		$query->bindValue(':descriptionzoo', $descriptionzoo, PDO::PARAM_STR);
		$query->bindValue(':prixadultezoo', $prixadultezoo, PDO::PARAM_INT);
		$query->bindValue(':prixenfantzoo', $prixenfantzoo, PDO::PARAM_INT);
		$query->bindValue(':payszoo', $payszoo, PDO::PARAM_STR);
		$query->bindValue(':villezoo', $villezoo, PDO::PARAM_STR);
		$query->bindValue(':urlzoo', $urlzoo, PDO::PARAM_STR);
		$query->bindValue(':imagezoo', $imagezoo, PDO::PARAM_STR);
		$query->bindValue(':cartezoo', $cartezoo, PDO::PARAM_STR);
		$query->execute();

		header('Location: ../html/ajouterZoo.html');
	}
	else
	{
		// si on retire le required
		echo 'Veuillez remplir tous les champs';
		header('Location: ../html/ajouterZoo.html');
	}
?>