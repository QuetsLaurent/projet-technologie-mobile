﻿drop database if exists bookazoo_projet;
create database if not exists bookazoo_projet character set ='utf8';
use bookazoo_projet;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `NUMCOMMANDE` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `IDUSER` int(11) NOT NULL,
  `IDZOO` tinyint(4) NOT NULL,
  `NOMBRETICKETENFANT` tinyint(3) UNSIGNED NOT NULL,
  `PRIXTICKETENFANT` smallint(5) UNSIGNED NOT NULL,
  `NOMBRETICKETADULTE` tinyint(3) UNSIGNED NOT NULL,
  `PRIXTICKETADULTE` smallint(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`NUMCOMMANDE`),
  KEY `fkcommandeutilisateur` (`IDUSER`),
  KEY `fkcommandezoo` (`IDZOO`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8;



INSERT INTO `commande` (`NUMCOMMANDE`, `IDUSER`, `IDZOO`, `NOMBRETICKETENFANT`, `PRIXTICKETENFANT`, `NOMBRETICKETADULTE`, `PRIXTICKETADULTE`) VALUES
(119, 66, 2, 0, 73, 4, 79);


DROP TABLE IF EXISTS `zoo`;
CREATE TABLE IF NOT EXISTS `zoo` (
  `IDZOO` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `NOMZOO` varchar(100) NOT NULL,
  `DESCRIPTIONZOO` varchar(1000) NOT NULL DEFAULT '',
  `PRIXADULTEZOO` int(11) NOT NULL,
  `PRIXENFANTZOO` int(11) NOT NULL,
  `PAYSZOO` varchar(50) NOT NULL,
  `VILLEZOO` varchar(100) NOT NULL,
  `URLZOO` varchar(200) NOT NULL,
  `IMAGEZOO` varchar(200) NOT NULL,
  `CARTEZOO` varchar(1000) NOT NULL,
  PRIMARY KEY (`IDZOO`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;



INSERT INTO `zoo` (`IDZOO`, `NOMZOO`, `DESCRIPTIONZOO`, `PRIXADULTEZOO`, `PRIXENFANTZOO`, `PAYSZOO`, `VILLEZOO`, `URLZOO`, `IMAGEZOO`, `CARTEZOO`) VALUES
(1, 'Zooparc de Beauval', '', 32, 25, 'France', 'Beauval', 'https://www.zoobeauval.com/', 'images/1.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d43333.22575411955!2d1.3097230903448691!3d47.24931632912208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47fb59099124b08d%3A0x7a84573c960121bf!2sZoo%20De%20Beauval!5e0!3m2!1sfr!2sbe!4v1586285371604!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'),
(2, 'Pairi Daiza', '', 34, 28, 'Belgique', 'Brugelette', 'https://www.pairidaiza.eu/en#pairi-daiza-resort', 'images/2.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2533.2661446701277!2d3.885127515270691!3d50.585006385541966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c25368a97eac47%3A0x103b807be4316746!2sPairi%20Daiza!5e0!3m2!1sfr!2sbe!4v1586285535206!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'),
(3, 'Zoo de Berlin', '', 13, 6.50, 'Allemagne', 'Berlin', 'https://www.zoo-berlin.de/en', 'images/3.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2428.4097771949546!2d13.33556601534988!3d52.50792284487731!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a851aabdc86f69%3A0x8139eb3ae980ae7e!2sZoo%20de%20Berlin!5e0!3m2!1sfr!2sbe!4v1586285749765!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'),
(4, 'Parc Safari de Beekse Bergen', '', 26, 22.50, 'Pays Bas', 'Beekse Bergen', 'https://www.beeksebergen.nl/fr', 'images/4.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9930.254388028932!2d5.109036335637948!3d51.5212218757424!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6bf813c2fe70f%3A0x8bb58fccaff5a081!2sBeekse%20Bergen!5e0!3m2!1sfr!2sbe!4v1586286214436!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'),
(5, 'Zoo d''Anvers', '', 20, 26.50, 'Belgique', 'Antwerpen', 'https://www.zooantwerpen.be/fr/', 'images/5.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2499.148348991238!2d4.421352715759305!3d51.2163427795884!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c3f7041c9505cf%3A0x21c2b3673eadbe1f!2sZoo%20d&#39;Anvers!5e0!3m2!1sfr!2sbe!4v1586286814840!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'),
(6, 'Planckendael', '', 27.50, 21, 'Belgique', 'Mechelen', 'https://www.zooplanckendael.be/', 'images/6.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21905.113668706825!2d4.503111212613016!3d50.99980132942214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c3e6af76970227%3A0x1e2d70317b3259e1!2sZoo%20de%20Planckendael!5e0!3m2!1sfr!2sbe!4v1586286507823!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'),
(7, 'Zoo de Schönbrunn', '', 21.50, 11, 'Autriche', 'Wien', 'https://www.viennapass.fr/vienna-attractions/schonbrunn-zoo.html', 'images/7.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5209.161741851085!2d6.1361476259817955!3d49.24643517118172!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479526548df6704f%3A0xf89e5f58c1aee926!2sZoo%20d&#39;Amn%C3%A9ville!5e0!3m2!1sfr!2sbe!4v1586286890875!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'),
(8, 'Zoo d''Amnéville', '', 37, 31, 'France', 'Amnéville', 'https://www.zoo-parc.fr/zoo-d-amneville/', 'images/8.jpg', '<iframe src=\'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2485.6907677819922!2d-0.6522161843446573!3d51.46383377962796!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48767b9ee7e8d33f%3A0x821725df6cb417bf!2sLEGOLAND%C2%AE+Windsor+Resort!5e0!3m2!1sfr!2sbe!4v1555524977322!5m2!1sfr!2sbe\' width=\'350\' height=\'250\' class=\'map\' allowfullscreen></iframe>'),
(9, 'Bioparc de Valencia', '', 23.80, 18, 'Espagne', 'Valencia', 'https://www.bioparcvalencia.es/', 'images/9.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6159.207474550772!2d-0.4096345!3d39.478279949999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd6045879cfacebb%3A0x5528071362f6c4af!2sBioparc%20Valencia!5e0!3m2!1sfr!2sbe!4v1586287231040!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>'),
(10, 'Zoo de Doué la Fontaine', '', 23.50, 17.50, 'France', 'Doué-en-Anjou', 'https://www.bioparc-zoo.fr/', 'images/10.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5951199.655001119!2d-4.844396078514174!3d43.25437248706957!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4807ea4113261043%3A0x2099d0cd23198d1c!2sBioparc%20de%20Dou%C3%A9-la-Fontaine!5e0!3m2!1sfr!2sbe!4v1586287167395!5m2!1sfr!2sbe" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>');


DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `IDUSER` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `MAILUSER` varchar(100) NOT NULL,
  `PSEUDOUSER` varchar(20) NOT NULL,
  `MDPUSER` varchar(100) NOT NULL,
  PRIMARY KEY (`IDUSER`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;



INSERT INTO `utilisateur` (`IDUSER`, `MAILUSER`, `PSEUDOUSER`, `MDPUSER`) VALUES
(62, 'test@hotmail.be', 'test', 'test'),
(68, 'admin@hotmail.be', 'admin', 'admin');
COMMIT;

alter table Commande add constraint fkcommandeutilisateur foreign key(IDUSER) references Utilisateur(IDUSER);
alter table Commande add constraint fkcommandezoo foreign key(IDZOO) references Parc(IDZOO);